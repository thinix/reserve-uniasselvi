# RESERVE-ME

Este projeto é uma solucao para: gerenciar reservas de horarios referente a utilizacao de quadra para a pratica de esportes.

## Credencial de usuario

A credencial padrao do usuario criado na inicializacao do sistema:

```
usuario: admin
senha  : admin
```

## Endpoints

| descricao | host      | porta | endpoint                           |
| --------- | --------- | ----- | ---------------------------------- |
| Frontend  | localhost | 5173  | http://localhost:5173              |
| Backend   | localhost | 8888  | http://localhost:8888              |
| Database  | localhost | 3306  | mysql://app@localhost:3306/reserve |

## Comandos

### Start

Para iniciar o projeto, execute o comando abaixo em um terminal, na pasta raiz deste projeto:

```bash
~/projetos/reserve-me> make start
# ou simplemente make
```

Ordem dos servicos que serão iniciados:

- servidor de banco de dados mysql, executado um script de seed em seguida
- servidor de aplicação com o php
- servidor http, que servirá arquivos estáticos js, html e css referente as telas do sistema

### Stop

Para parar todos os serviços iniciados pelo comando start, execute o comando abaixo em um terminal, na pasta raiz deste projeto:

```bash
~/projetos/reserve-me> make stop
# os servicos serao parados na ordem correta
```

### Outros comandos

Para executar os comandos abaixo, em um terminal entre na pasta raiz deste projeto e execute:

```bash
~/projetos/reserve-me> make <comando>
```

| comando        | descricao                                                                                                                                        |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| pull           | baixa todas as imagens docker necessarias para executar este projeto                                                                             |
| start.database | inicia o servidor de banco de dados mysql                                                                                                        |
| start.backend  | inicia o servidor de aplicação com o php, baixa os fontes das libs PDO e PDO_MYSQL, compila e habilta para ser utilizado no php                  |
| start.frontend | baixa as dependencias do frontend, gera os arquivos estaticos html, css e js e inicia um servidor http para servir os arquivos estaticos gerados |
| seed.mysql     | conecta no banco de dados mysql e executa um script sql para criar a estrutura basica                                                            |
| cli.mysql      | conecta em um shell interativo para executar comandos no banco de dados mysql                                                                    |

exemplo:

```bash
~/projetos/reserve-me> make pull
# para baixar todas as imagens docker necessarias para executar este projeto
```

## Programas, linguagens de programacao e libs utilizadas nesse projeto

### Essenciais

Ordenado por nome

| tipo     | nome          | link                                                       |
| -------- | ------------- | ---------------------------------------------------------- |
| frontend | Bootstrap 5.2 | https://getbootstrap.com                                   |
| frontend | CSS           | https://www.w3schools.com/css/css_intro.asp                |
| infra    | Docker        | https://docs.docker.com/get-started/overview               |
| frontend | HTML          | https://www.w3schools.com/html/html_intro.asp              |
| frontend | Javascript    | https://developer.mozilla.org/en-US/docs/Web/JavaScript    |
| infra    | Make          | https://www.gnu.org/software/make                          |
| database | MySQL 8       | https://dev.mysql.com/doc/refman/8.0/en/what-is-mysql.html |
| backend  | PHP 8.1       | https://www.php.net/releases/8.1/en.php                    |

### Libs

- [Fullcalendar](https://fullcalendar.io/)

### Suporte ao desenvolvimento

- [Git 2](https://git-scm.com/about)
- [Gitlab](https://about.gitlab.com)
- [Vite 3](https://vitejs.dev)
- [VSCode](https://code.visualstudio.com)

## Captura de telas

### Login

![tela de login](/doc/images/login.png "tela de login, acesso ao sistema")

### Listagem dos agendamentos

![listagem de agendamento](/doc/images/telaprincipal-listagem.png "tela principal, listagem dos agendamentos")

### Agendamentos por dia

![agendamentos exibidos por dia](/doc/images/telaprincipal-pordia.png "tela principal, listagem dos agendamentos exibidos por dia")

### Agendamentos por semana

![agendamentos exibidos por semana](/doc/images/telaprincipal-porsemana.png "tela principal, listagem dos agendamentos exibidos por semana")

### Agendamentos por mes

![agendamentos exibidos por mes](/doc/images/telaprincipal-pormes.png "tela principal, listagem dos agendamentos exibidos por mes")

### Novo reserva

![tela para criar uma nova reserva](/doc/images/novoagendamento.png "tela nova reserva, para criar novas reserva")
