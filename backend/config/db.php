<?php

$dbhost="database-srv";
$dbport=3306;

$dbname="reserve";

$dbuser="app";
$dbpass="123456";

try {
    $db = new PDO("mysql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);   
} catch (PDOException $e) {
    print "#ERRO nao foi possivel conectar ao banco de dados!";
    print $e->getMessage();
    die();
} finally {
    $dbhost=null;
    $dbport=null;
    $dbname=null;
    $dbuser=null;
    $dbpass=null;
}