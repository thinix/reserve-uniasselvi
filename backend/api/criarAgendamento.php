<?php
header('Access-Control-Allow-Origin:*');
header("Content-Type:application/json");

$response['error'] = true;
$response['errorMessage'] = null;
$response['message'] = 'nao foi possivel criar o agendamento';
$response['status'] = 500;

$response['data'] = null;

http_response_code($response['status']);

if($_SERVER['REQUEST_METHOD'] !== "POST") {
    $response['status'] = 400;
    http_response_code($response['status']);
    echo json_encode($response);
    die();
}

$inputJson = file_get_contents('php://input');
$inputData = json_decode($inputJson, true);

if (count($inputData) !== 4 || $inputData['usuario'] === '' || $inputData['inicio'] === '' || $inputData['fim'] === '') {
    $response['status'] = 400;
    http_response_code($response['status']);
    echo json_encode($response);
    die();
}

require('/backend/config/db.php');

$inputToken = $inputData['usuario'];
$inputQuadraId = $inputData['quadra'];
$inputInicio = $inputData['inicio'];
$inputFim = $inputData['fim'];

try {
    if(!$inputToken || $inputToken == '') {
        $response['errorMessage'] = "usuario nao autenticado";
        $response['status'] = 401;
        http_response_code($response['status']);
        echo json_encode($response);
        die();    
    }

    if($inputQuadraId <= 0) {
        $response['errorMessage'] = "quadra nao foi informada";
        $response['status'] = 400;
        http_response_code($response['status']);
        echo json_encode($response);
        die();    
    }

    if(!$inputInicio || !$inputFim) {
        $response['errorMessage'] = "periodo nao foi informada";
        $response['status'] = 400;
        http_response_code($response['status']);
        echo json_encode($response);
        die();    
    }

    $stmt = $db->prepare("SELECT id FROM usuarios WHERE token_acesso = :token");
    $stmt->execute([ 'token' => $inputToken ]);

    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    $usuarioId = $data ? $data['id'] : 0;

    if(count($data) !== 1 || $usuarioId <= 0) {
        $response['errorMessage'] = "sessao nao encontrada";
        $response['status'] = 401;
        http_response_code($response['status']);
        echo json_encode($response);
        die();    
    }

    $stmt = $db->prepare("SELECT 1 FROM agendamentos WHERE quadra = :quadra AND (data_hora_inicial BETWEEN :inicio AND :fim OR data_hora_final BETWEEN :inicio AND :fim )");
    $stmt->execute([ 'quadra' => $inputQuadraId, 'inicio' => $inputInicio, 'fim' => $inputFim ]);
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    if(count($data) > 0){
        $response['errorMessage'] = "ja existe um agendamento para essa quadra no periodo informado";
        $response['status'] = 422;
        http_response_code($response['status']);
        echo json_encode($response);
        die();    
    }

    $stmt = $db->prepare("INSERT INTO agendamentos(usuario, quadra, data_hora_inicial, data_hora_final) VALUES(:usuario, :quadra, :inicio, :fim)");
    $stmt->execute([ 'usuario' => $usuarioId, 'quadra' => $inputQuadraId, 'inicio' => $inputInicio, 'fim' => $inputFim]);

    $response['error'] = false;
    $response['errorMessage'] = null;
    $response['message'] = 'agendamento cadastrado no sistema';
    $response['status'] = 200;
} catch (Exception $e) {
    $response['error'] = true;
    $response['errorMessage'] = $e->getMessage();
    $response['message'] = 'nao foi possivel recuperar a listagem de agendamentos';
    $response['status'] = 500;
    //
    $response['data'] = null;
} finally {
    $stmt->closeCursor();
    $stmt = null; // liberar recursos utilizados
    $db = null; // fechar conexao
}

http_response_code($response['status']);
echo json_encode($response);
