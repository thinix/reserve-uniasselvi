<?php
header('Access-Control-Allow-Origin:*');
header("Content-Type:application/json");

require('/backend/config/db.php');

$response['error'] = true;
$response['errorMessage'] = null;
$response['message'] = 'nao foi possivel recuperar a listagem de agendamentos';
$response['status'] = 500;
//
$response['data'] = null;

http_response_code($response['status']);

try {
    $stmt = $db->query("
    SELECT
        a.id agendamento,
        a.usuario, u.nome usuarioNome,
        a.quadra, q.nome quadraNome,
        a.data_hora_inicial, a.data_hora_final
    FROM agendamentos a
    JOIN usuarios u ON u.id = a.usuario
    JOIN quadras q ON q.id = a.quadra
    ORDER BY data_hora_inicial
");

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    $response['error'] = false;
    $response['errorMessage'] = null;
    $response['message'] = 'agendamentos cadastrados no sistema';
    $response['status'] = 200;
    //
    $response['count'] = count($data);
    $response['data'] = $data;
} catch (Exception $e) {
    $response['error'] = true;
    $response['errorMessage'] = $e->getMessage();
    $response['message'] = 'nao foi possivel recuperar a listagem de agendamentos';
    $response['status'] = 500;
    //
    $response['data'] = null;
} finally {
    $stmt->closeCursor();
    $stmt = null; // liberar recursos utilizados
    $db = null; // fechar conexao
}

http_response_code($response['status']);
echo json_encode($response);
