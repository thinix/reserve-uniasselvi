<?php
header('Access-Control-Allow-Origin:*');
header("Content-Type:application/json");

$response['accessToken'] = null;
$response['error'] = true;
$response['errorMessage'] = 'unauthorized';
$response['message'] = 'acesso nao autorizado';
$response['status'] = 401;

http_response_code($response['status']);

if($_SERVER['REQUEST_METHOD'] !== "POST") {
    $response['status'] = 400;
    http_response_code($response['status']);
    echo json_encode($response);
    die();
}

$inputJson = file_get_contents('php://input');
$inputData = json_decode($inputJson, true);

if (count($inputData) !== 2 || $inputData['username'] === '' || $inputData['password'] === '') {
    $response['status'] = 400;
    http_response_code($response['status']);
    echo json_encode($response);
    die();
}

require('../config/db.php');
require('../utils/guid.php');

$inputUsername = $inputData['username'];
$inputPassword = $inputData['password'];
$credential = "$inputUsername:$inputPassword";

try {
    $stmt = $db->prepare('SELECT id, nome FROM usuarios WHERE credencial = :credential');
    $stmt->execute([ 'credential' => $credential ]);

    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    $userId = $user ? $user['id'] : 0;
    $username = $user ? $user['nome'] : '';

    if(!$user || !($userId > 0) || $username !== $inputUsername) {
        echo json_encode($response);
        die();   
    }

    $accessToken = guidv4();
    $stmt = $db->prepare('UPDATE usuarios SET token_acesso = :token WHERE id = :id');
    $stmt->execute([ 'token' => $accessToken, 'id' => $userId ]);

    $response['accessToken'] = $accessToken;
    $response['error'] = false;
    $response['errorMessage'] = null;
    $response['message'] = 'acesso autorizado';
    $response['status'] = 200;
} catch (Exception $e) {
    $response['accessToken'] = null;
    $response['error'] = true;
    $response['errorMessage'] = $e->getMessage();
    $response['message'] = 'nao foi possivel realizar o login';
    $response['status'] = 500;
} finally {
    $stmt->closeCursor();
    $sth = null; // liberar recursos utilizados
    $db = null; // fechar conexao
}

http_response_code($response['status']);
echo json_encode($response);
