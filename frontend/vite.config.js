export default {
  publicDir: false,
  resolve: {
    alias: {
      "~bootstrap": "./node_modules/bootstrap",
      "~bootstrap-icons": "./node_modules/bootstrap-icons",
    },
  },
};
