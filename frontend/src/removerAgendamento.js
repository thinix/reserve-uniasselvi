import { apiBaseUrl } from "./config";

const endpoint = `${apiBaseUrl}/removerAgendamento.php`;

export async function removerAgendamento(id) {
  try {
    const response = await fetch(`${endpoint}?agendamento=${id}`);
    if (response.ok) {
        alert("agendamento removido com sucesso!");
    }
  } catch (err) {
    console.log("#ERROR remover agendamento", err.message, err.stack);
  }
}
