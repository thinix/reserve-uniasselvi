import { apiBaseUrl } from "./config";

const endpoint = `${apiBaseUrl}/quadras.php`;

export async function fetchQuadras() {
  try {
    const response = await fetch(endpoint);
    if (response.ok) {
      const responseData = await response.json();

      if (Array.isArray(responseData.data)) {
        const { data } = responseData;
        return data;
      }
    }
  } catch (err) {
    console.log("#ERROR fetch quadras", err.message, err.stack);
  }

  return [];
}
