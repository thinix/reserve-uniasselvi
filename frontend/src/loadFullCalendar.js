import { FullCalendar } from "./assets/fullcalendar/main.js";
import { loadCalendarData } from "./loadCalendarData.js";
import { removerAgendamento } from "./removerAgendamento.js";

export async function loadFullCalendar() {
  const calendarEl = document.getElementById("calendario");
  const mainCalendar = new FullCalendar.Calendar(calendarEl, {
    initialView: "listWeek", // "dayGridMonth",
    nowIndicator: true,
    locale: {
      code: "pt-br",
      buttonText: {
        prev: "Anterior",
        next: "Pr\xF3ximo",
        today: "Hoje",
        month: "M\xEAs",
        week: "Semana",
        day: "Dia",
        list: "Lista",
      },
      weekText: "Sm",
      allDayText: "dia inteiro",
      moreLinkText: function (n) {
        return "mais +" + n;
      },
      noEventsText: "N\xE3o h\xE1 eventos para mostrar",
    },
    dateClick(e) {
      console.log(e);
      alert(`voce clicou em ${e.date.toLocaleDateString()}`);
    },
    eventClick: async function (info) {
      const response = confirm(`Deseja excluir "${info.event.title}"`);
      if (response) {
        removerAgendamento(info.event.extendedProps.agendamentoId)
        this.getEvents().forEach((event) => {
          event.remove();
        });
        await loadCalendarData(mainCalendar);
      }
    },
  });
  mainCalendar.render();

  await loadCalendarData(mainCalendar);
  mainCalendar.gotoDate(new Date());

  return mainCalendar;
}
