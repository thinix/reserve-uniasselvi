import { apiBaseUrl } from "./config";

const endpoint = `${apiBaseUrl}/agendamentos.php`;

export async function loadCalendarData(calendar) {
  if (!calendar) return;
  try {
    const response = await fetch(endpoint);
    if (response.ok) {
      const responseData = await response.json();

      if (Array.isArray(responseData.data)) {
        const { data } = responseData;

        data.forEach((item) => {
          calendar.addEvent({
            agendamentoId: item.agendamento,
            title: `${item.quadraNome} - ${item.usuarioNome}`,
            start: item.data_hora_inicial,
            end: item.data_hora_final,
          });
        });

        return data;
      }
    }
  } catch (err) {
    console.log("#ERROR loadCalendarData", err.message, err.stack);
  }

  return [];
}
