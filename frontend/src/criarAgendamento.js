import { apiBaseUrl } from "./config";

export async function criarAgendamento(e, postCriarAgendamentoCallback) {
  e.stopPropagation();
  e.preventDefault();

  const reservasView = document.getElementById("reservasView");

  const quadraFormCriarReservaEl = document.getElementById(
    "quadraFormCriarReserva"
  );
  const inicioFormCriarReservaEl = document.getElementById(
    "dataHoraInicialFormCriarReserva"
  );
  const fimFormCriarReservaEl = document.getElementById(
    "dataHoraFinalFormCriarReserva"
  );

  const accessToken = localStorage.getItem("reserve:token");

  try {
    const endpoint = `${apiBaseUrl}/criarAgendamento.php`;
    const options = {
      method: "POST",
      body: JSON.stringify({
        usuario: localStorage.getItem("reserve:token"),
        quadra: quadraFormCriarReservaEl.value,
        inicio: inicioFormCriarReservaEl.value,
        fim: fimFormCriarReservaEl.value,
      }),
    };
    const response = await fetch(endpoint, options);
    const responseData = await response.json();
    if (response.ok) {
      if (responseData.error) {
        alert(responseData.errorMessage);
      }
      alert("reserva criada com sucesso!");
    } else if (responseData.error) {
      alert(
        `${responseData.message || ""} ${responseData.errorMessage || ""}`
      );
    } else if (responseData.message) {
      alert(`${responseData.message}`);
    }
  } catch (err) {
    console.log(err);
    alert("erro ao criar reserva");
  }

  if (postCriarAgendamentoCallback) {
    postCriarAgendamentoCallback();
  }
}
