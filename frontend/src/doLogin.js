import { apiBaseUrl } from "./config";

export async function doLogin(postLoginCallback) {
  const entrarLoginEl = document.getElementById("entrarLogin");
  const usernameFormLoginEl = document.getElementById("usernameFormLogin");
  const passwordFormLoginEl = document.getElementById("passwordFormLogin");

  entrarLoginEl.classList.toggle("disabled", true);

  if (!usernameFormLoginEl.value) {
    entrarLoginEl.classList.toggle("disabled", false);
    return usernameFormLoginEl.focus();
  }

  if (!passwordFormLoginEl.value) {
    entrarLoginEl.classList.toggle("disabled", false);
    return passwordFormLoginEl.focus();
  }

  const username = usernameFormLoginEl.value;
  const password = passwordFormLoginEl.value;

  usernameFormLoginEl.disabled = true;
  passwordFormLoginEl.disabled = true;
  localStorage.removeItem("reserve:token");

  try {
    const endpoint = `${apiBaseUrl}/login.php`;
    const options = {
      method: "POST",
      body: JSON.stringify({ username, password }),
    };

    const response = await fetch(endpoint, options);

    if (!response.ok) {
      console.log(await response.text());
      alert("credencial inválida!");
      return;
    }

    const responseData = await response.json();
    localStorage.setItem("reserve:token", responseData.accessToken);

    if (postLoginCallback) {
      postLoginCallback();
    }
  } catch (err) {
    console.log("#ERROR doLogin", err.message);
    if (postLoginCallback) {
      postLoginCallback(err);
    }
  } finally {
    usernameFormLoginEl.disabled = false;
    passwordFormLoginEl.disabled = false;
    entrarLoginEl.classList.toggle("disabled", false);
  }

  // entrarBotaoHeaderEl.classList.toggle("d-none", false);
  // mainEl.innerHTML = reservasView.innerHTML;
}
