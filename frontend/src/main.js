import * as bootstrap from "bootstrap";

import { loadFullCalendar } from "./loadFullCalendar.js";
import { doLogin } from "./doLogin.js";

import { criarAgendamento } from "./criarAgendamento";
import { fetchQuadras } from "./fetchQuadras.js";

// # INICIAR APP

const mainEl = document.getElementsByTagName("main")[0];
const sairBotaoHeaderEl = document.getElementById("sairBotaoHeader");

const loginView = document.getElementById("loginView");
const reservasView = document.getElementById("reservasView");
const criarReservaView = document.getElementById("criarReservaView");

const accessToken = localStorage.getItem("reserve:token");

if (accessToken) {
  exibirReservasView();
} else {
  exibirLoginView();
}

sairBotaoHeaderEl.addEventListener("click", function (e) {
  e.preventDefault();
  sairBotaoHeaderEl.classList.toggle("d-none", true);
  localStorage.clear();
  mainEl.innerHTML = loginView.innerHTML;
});

function exibirLoginView() {
  sairBotaoHeaderEl.classList.toggle("d-none", true);

  mainEl.innerHTML = loginView.innerHTML;

  const formLoginEl = document.getElementById("formLogin");
  const entrarLoginEl = document.getElementById("entrarLogin");

  function _doLogin(e) {
    e.preventDefault();

    doLogin(exibirReservasView);
  }

  formLoginEl.addEventListener("submit", _doLogin);
  entrarLoginEl.addEventListener("click", _doLogin);
}

function exibirReservasView(err) {
  if (!localStorage.getItem("reserve:token")) {
    return exibirLoginView();
  }

  sairBotaoHeaderEl.classList.toggle("d-none", false);
  if (!err) {
    mainEl.innerHTML = reservasView.innerHTML;
    (async () => (window.mainCalendar = await loadFullCalendar()))();

    const novaReservaBotao = document.getElementById("novaReserva");
    novaReservaBotao.addEventListener("click", exibirNovaReservaView);
  }
}

async function exibirNovaReservaView() {
  app.innerHTML = criarReservaView.innerHTML;

  const quadraFormCriarReservaEl = document.getElementById(
    "quadraFormCriarReserva"
  );
  const formCriarReservaEl = document.getElementById("formCriarReserva");
  const criarReservaEl = document.getElementById("criarReserva");
  const dataHoraInicialFormCriarReservaEl = document.getElementById(
    "dataHoraInicialFormCriarReserva"
  );
  const dataHoraFinalFormCriarReservaEl = document.getElementById(
    "dataHoraFinalFormCriarReserva"
  );

  let dataHoraAtual = new Date().toISOString().slice(0, 16);
  let [data, hora, minuto] = dataHoraAtual.split(/[T:]/);
  hora = hora - 3;
  if (hora < 0) {
    hora = 23 + hora;
  }
  dataHoraInicialFormCriarReservaEl.value = `${data}T${hora}:00`;
  try {
    const quadras = await fetchQuadras();
    quadras.forEach((quadra) => {
      quadraFormCriarReservaEl.add(new Option(quadra.nome, quadra.id));
    });
  } catch (err) {
    console.log("#ERRO fetch quadras", err.message);
    alert("erro ao recuperar a lista de quadras");
  }

  criarReservaEl.addEventListener("click", (e) => {
    let [data, hora, minuto] =
      dataHoraInicialFormCriarReservaEl.value.split(/[T:]/);
// debugger;
    let dataHoraFinal = Number(new Date(`${data}T${hora.toString().padStart(2,'0')}:${minuto.padStart(2,'0')}`));
    dataHoraFinal = new Date(dataHoraFinal + 1000 * 60 * 60).toISOString();
    [data, hora, minuto] = dataHoraFinal.split(/[T:]/);
    hora = hora - 3;
    if (hora < 0) {
      hora = 23 + hora;
    }
    dataHoraFinalFormCriarReservaEl.value = `${data}T${hora.toString().padStart(2,'0')}:${minuto.padStart(2,'0')}`;

    criarAgendamento(e, exibirReservasView);
  });
}
