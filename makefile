start:
	@make pull
	@-make stop
	@make start.database
	@make start.backend
	@make start.frontend
	make seed.mysql; while [ $$? -gt 0 ]; do sleep 5 && make seed.mysql; done
	@[ $HOSTNAME == "srv00.rocha.uk" ] && make start.proxy || true
	docker ps
#	docker stats || make stop

stop:
	-docker stop proxy-srv
	-docker stop frontend-srv
	-docker stop backend-srv
	-docker stop database-srv


pull:
	docker pull docker.io/library/mysql:8
	docker pull docker.io/library/php:8-alpine
	docker pull docker.io/library/node:18-alpine
	docker pull docker.io/library/haproxy:2.7-alpine


start.database:
	mkdir -p $(PWD)/database/data
	-docker network create reserve-net
	docker run --rm -it --name database-srv \
		--hostname database-srv \
		-e MYSQL_ROOT_PASSWORD=123456789 \
		-e MYSQL_USER=app \
		-e MYSQL_PASSWORD=123456 \
		-e MYSQL_DATABASE=reserve \
		-v "$(PWD)/database/data:/var/lib/mysql:z" \
		-v "$(PWD)/database/seed:/database/seed:z" \
		-p 3306:3306 \
		--network reserve-net \
	-d docker.io/library/mysql:8

start.backend:
	-docker network create reserve-net
	docker run --rm -it --name backend-srv --user root \
		--hostname backend-srv \
		-v "$(PWD)/backend:/backend:z" \
		-v "$(PWD)/backend/docker-php-entrypoint:/usr/local/bin/docker-php-entrypoint:z" \
		-p 8888:8888 \
		--network reserve-net \
	-d docker.io/library/php:8-alpine php -S 0.0.0.0:8888 -t /backend/api

start.frontend:
	docker rm -f frontend-srv
	make install.frontend
	docker run --rm -ti --name frontend-build-srv \
		-e "VITE_API_BASE_URL=http://reserve-api.rocha.uk:8888" \
		-v "$(PWD)/frontend:/app" \
		-w "/app" \
	docker.io/library/node:18-alpine npm run build
	docker run --rm -ti --name frontend-srv \
		-h frontend-srv \
		-v "$(PWD)/frontend:/app" \
		-p 4173:4173 \
		-w "/app" \
	-d docker.io/library/node:18-alpine npm run preview -- --host

start.proxy:
#	docker container stop proxy-srv
	docker container run --name proxy-srv \
		--detach \
		-v ${PWD}/proxy/haproxy:/usr/local/etc/haproxy \
		--network reserve-net \
		-p 80:80 \
		-p 443:443 \
	haproxy:2.7-alpine


install.frontend:
	docker run --rm -ti --name frontend-install-srv \
		-v "$(PWD)/frontend:/app" \
		-w "/app" \
	docker.io/library/node:18-alpine npm install

dev.frontend:
	make install.frontend
	docker run --rm -ti --name frontend-dev-srv \
		-v "$(PWD)/frontend:/app" \
		-p 5173:5173 \
		-w "/app" \
	docker.io/library/node:18-alpine npm run dev -- --host


seed.mysql:
	docker exec -ti -e MYSQL_PWD=123456789 database-srv sh /database/seed/execute

cli.mysql:
	docker exec -ti \
		-e MYSQL_HOST=localhost \
		-e MYSQL_TCP_PORT=3306 \
		-e MYSQL_PWD=123456789 \
	database-srv mysql reserve

stats:
	docker stats
