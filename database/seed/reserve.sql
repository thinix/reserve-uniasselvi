CREATE TABLE IF NOT EXISTS usuarios (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL,
    credencial VARCHAR(100) NOT NULL,
    token_acesso VARCHAR(100) NULL
);

CREATE TABLE IF NOT EXISTS quadras (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(150) NOT NULL
);

COMMIT;

CREATE TABLE IF NOT EXISTS agendamentos (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,

    usuario INT NOT NULL,
    quadra INT NOT NULL,
    data_hora_inicial TIMESTAMP NOT NULL,
    data_hora_final TIMESTAMP NOT NULL,

    CONSTRAINT agendamentos_fk_usuarios
    FOREIGN KEY (usuario)
    REFERENCES usuarios (id),

    CONSTRAINT agendamentos_fk_quadras
    FOREIGN KEY (quadra)
    REFERENCES quadras (id)
);

COMMIT;

INSERT INTO usuarios(nome, credencial)
SELECT 'admin', 'admin:admin'
WHERE NOT EXISTS(
    SELECT 1 FROM usuarios WHERE nome = 'admin'
);

INSERT INTO quadras(nome)
SELECT 'quadra futebol'
WHERE NOT EXISTS(
    SELECT 1 FROM quadras
    WHERE nome = 'quadra futebol'
)
UNION
SELECT 'quadra tenis'
WHERE NOT EXISTS(
    SELECT 1 FROM quadras
    WHERE nome = 'quadra tenis'
);

INSERT INTO agendamentos(usuario, quadra, data_hora_inicial, data_hora_final)
SELECT 1, 1, '2022-11-01 08:00:00-03:00', '2022-11-01 10:00:00-03:00'
WHERE NOT EXISTS(
    SELECT 1 FROM agendamentos
    WHERE id = 1
);

INSERT INTO agendamentos(usuario, quadra, data_hora_inicial, data_hora_final)
SELECT 1, 2, '2022-11-01 16:00:00-03:00', '2022-11-01 18:00:00-03:00'
WHERE NOT EXISTS(
    SELECT 1 FROM agendamentos
    WHERE id = 2
);

COMMIT;
